import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./Modules/card/card.module').then(m => m.CardModule)
  },
  {
    path: 'weather-details',
    loadChildren: () => import('./Modules/weather-details/weather-details.module').then(m => m.WeatherDetailsModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
