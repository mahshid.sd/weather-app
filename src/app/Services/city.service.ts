import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { City } from '../Models/city.model';


@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(private http: HttpClient) { }

  getCity(city: any):Observable<City> {
    return this.http.get<any>(`http://api.openweathermap.org/data/2.5/weather?q=${city},&appid=257f3be5d6eb9d0ed37c9a9398d0a3eb`);
  }

  getCityDetails(city: string, cnt: number): Observable<any> {
    return this.http.get(`http://api.openweathermap.org/data/2.5/forecast/?q=${city}&cnt=${cnt}&units=metric&APPID=257f3be5d6eb9d0ed37c9a9398d0a3eb`);
  }
}
