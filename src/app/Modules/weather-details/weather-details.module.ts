import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WeatherDetailsRoutingModule } from './weather-details-routing.module';

import { MatCardModule } from '@angular/material/card';

import { CityPageComponent } from './Component/city-page/city-page.component';

@NgModule({
  declarations: [
    CityPageComponent
  ],
  imports: [
    CommonModule,
    WeatherDetailsRoutingModule,
    MatCardModule,
  ]
})
export class WeatherDetailsModule { }
