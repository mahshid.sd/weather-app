import {
  Component,
  OnInit
} from '@angular/core';
import {
  ActivatedRoute
} from '@angular/router';

import {
  City
} from '../../../../Models/city.model';

import {
  CityService
} from '../../../../Services/city.service';

@Component({
  selector: 'app-city-page',
  templateUrl: './city-page.component.html',
  styleUrls: ['./city-page.component.scss']
})
export class CityPageComponent implements OnInit {

  city: City;
  name: string;
  details: any;
  weekday = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
  days = [];
  DaysDetails: any[] = [];

  constructor(private route: ActivatedRoute, private cityService: CityService) {}

  ngOnInit(): void {
    this.getCityName();
  }

  getCityName() {
    this.route.paramMap.subscribe(params => {
      this.name = params.get('name');
      this.getCity(this.name);
    });
  }

  getCity(city: string) {
    this.cityService.getCity(city).subscribe(city => {
      this.city = city;
      this.getCityDetails(this.city.name, 5);
    })
  }

  getCityDetails(city: string, cnt: number) {
    this.get5Days();
    this.cityService.getCityDetails(city, cnt).subscribe(city => {
      this.details = city;
      this.details = this.details.list;
      for (let i = 0; i < 5; i++) {
        this.DaysDetails[i] = {
          day: this.days[i],
          details: this.details[i],
          icon: "http://openweathermap.org/img/wn/" + this.details[i].weather[0].icon + "@4x.png"
        }
      }
    })
  }



  get5Days() {
    var day = new Date();
    var today = day.getDay() + 1;
    for (let i = 0; i < 5; i++) {
      if (today <= 6) {
        this.days.push(this.weekday[today])
        today += 1;
      }
      if (today > 6) {
        today = 0;
        this.days.push(this.weekday[today]);
        today += 1;
      }
    }
  }
}
