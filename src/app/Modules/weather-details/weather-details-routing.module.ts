import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CityPageComponent } from './Component/city-page/city-page.component';


const routes: Routes = [
  { path: ':name', component: CityPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WeatherDetailsRoutingModule { }
