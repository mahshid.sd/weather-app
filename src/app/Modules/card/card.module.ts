import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CardRoutingModule } from './card-routing.module';
import { FToCPipe } from './f-to-c.pipe';

import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';

import { AddCityCardComponent } from './Components/add-city-card/add-city-card.component';
import { CityCardComponent } from './Components/city-card/city-card.component';
import { CardsComponent } from './Components/cards/cards.component';
import { AddCityComponent } from './Components/add-city/add-city.component';



@NgModule({
  declarations: [
    AddCityCardComponent,
    CityCardComponent,
    CardsComponent,
    AddCityComponent,
    FToCPipe
  ],
  imports: [
    CommonModule,
    CardRoutingModule,
    MatCardModule,
    MatIconModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    MatButtonModule
  ]
})
export class CardModule { }
