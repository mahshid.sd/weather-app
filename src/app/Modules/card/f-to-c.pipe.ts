import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'FToC'
})
export class FToCPipe implements PipeTransform {

  transform(value: number) {
    return (value - 273.5).toFixed(1);
  }

}
