import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CardsComponent } from './Components/cards/cards.component';
import { AddCityComponent } from './Components/add-city/add-city.component';


const routes: Routes = [
  { path: '', component: CardsComponent},
  { path: 'add-city', component:  AddCityComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardRoutingModule { }
