import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { MatSnackBar} from '@angular/material/snack-bar';

import { City } from '../../../../Models/city.model';

@Component({
  selector: 'city-card',
  templateUrl: './city-card.component.html',
  styleUrls: ['./city-card.component.scss']
})
export class CityCardComponent implements OnInit {
  constructor(private router: Router, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  @Input() city: City;

  cities: City[]= [];

  onCityClick(city: City) {
    this.router.navigate(['/weather-details', city.name])
  }

  onAddClick() {
    localStorage.setItem(JSON.stringify(this.city.id), JSON.stringify(this.city.name));
    this.openSnackBar('City Added successfully!', 'ok');
    this.router.navigate([''])
  }

  onDeleteClick() {
    localStorage.removeItem(JSON.stringify(this.city.id))
    this.router.navigate([''])
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open( message, action, {
      duration: 1000,
      horizontalPosition: 'start',
      verticalPosition: 'bottom',
    });
  }
}
