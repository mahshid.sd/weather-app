import { Component, OnInit } from '@angular/core';
import { City } from 'src/app/Models/city.model';

import { CityService } from '../../../../Services/city.service';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {

  constructor(private cityService: CityService) { }

  ngOnInit(): void {
    this.getCities();
  }
  data: any;
  cities: City[] = [];
  city: City;


  getCityName(id: number) {
    return JSON.parse(localStorage.getItem(localStorage.key(id)))
  }

  getCities() {
    for(var i = 0; i < localStorage.length; i++){
      this.getCity(this.getCityName(i));
    }
  }

  getCity(city: string) {
    this.cityService.getCity(city)
    .subscribe(city =>
      {
        this.city = city;
        this.city.weather[0].icon = `http://openweathermap.org/img/wn/${this.city.weather[0].icon}@4x.png`
        this.cities.push(this.city);
      })
  }
}
