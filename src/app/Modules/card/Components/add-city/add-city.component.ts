import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { MatSnackBar} from '@angular/material/snack-bar';

import { CityService } from '../../../../Services/city.service';

import { City } from '../../../../Models/city.model';

@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.scss']
})
export class AddCityComponent implements OnInit {

  constructor(private cityService: CityService, private router: Router, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  city : City;
  name = new FormControl('');
  data: any;

  getCity(city: any) {
    this.cityService.getCity(city)
    .subscribe(city =>
      {
        this.city = city;
        this.city.weather[0].icon = `http://openweathermap.org/img/wn/${this.city.weather[0].icon}@4x.png`
      },
      error => this.openSnackBar(error.error.message, 'ok')
      )
  }

  onSearchClick() {
    this.getCity(this.name.value);
    this.name.reset();
  }

   openSnackBar(message: string, action: string) {
    this._snackBar.open( message, action, {
      duration: 1000,
      horizontalPosition: 'start',
      verticalPosition: 'bottom',
    });
  }
}
