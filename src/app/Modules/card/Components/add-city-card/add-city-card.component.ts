import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'add-city-card',
  templateUrl: './add-city-card.component.html',
  styleUrls: ['./add-city-card.component.scss']
})
export class AddCityCardComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onAddCity() {
    this.router.navigate(['add-city'])
  }
}
